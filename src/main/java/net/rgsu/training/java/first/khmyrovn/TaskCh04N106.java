package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh04N106 {
    private static void currentSeason() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите номер числа месяца: ");
        int month = scanner.nextInt();
        System.out.print(month + " месяц. Время года: ");
        String season;
        switch (month) {
            case 12:
            case 1:
            case 2:
                season = "Зима";
                break;
            case 3:
            case 4:
            case 5:
                season = "Весна";
                break;
            case 6:
            case 7:
            case 8:
                season = "Лето";
                break;
            case 9:
            case 10:
            case 11:
                season = "Осень";
                break;
            default:
                season = "ВЫМЫШЛЕННЫЙ МЕСЯЦ";
        }
        System.out.println(season);
    }

    public static void main(String[] args) {
        currentSeason();
    }
}
