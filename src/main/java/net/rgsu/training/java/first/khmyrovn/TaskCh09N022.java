package net.rgsu.training.java.first.khmyrovn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TaskCh09N022 {
    private static void printHalfOfWord() throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите слово, состоящее из чётного числа букв: ");
        String word = r.readLine();
        char[] mas = word.toCharArray();
        String s = String.copyValueOf(mas, 0, mas.length / 2);
        System.out.println("Половина слова: " + s);
    }

    public static void main(String[] args) throws IOException {
        printHalfOfWord();
    }
}
