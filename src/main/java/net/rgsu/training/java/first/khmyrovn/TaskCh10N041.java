package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh10N041 {
    private static int factNumber(int n) {
        if (n == 1)
            return 1;
        else
            return factNumber(n - 1) * n;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите натуральное число: ");
        int ourNumber = scanner.nextInt();
        System.out.print("Факториал числа " + ourNumber + " = ");
        long factorialOurNumber = factNumber(ourNumber);
        System.out.println(factorialOurNumber);
    }
}
