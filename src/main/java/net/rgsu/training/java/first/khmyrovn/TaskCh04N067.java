package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh04N067 {
    private static void dayOfWeekend() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите текущий день в году: ");
        int k = scanner.nextInt();
        int a, b, c, d, e, f, g;
        String result = null;
        for (a = 1, b = 2, c = 3, d = 4, e = 5, f = 6, g = 7; a <= 365; ) {
            if (k == a || k == b || k == c || k == d || k == e)
                result = "Workday";
            else if (k == f || k == g)
                result = "Weekend";
            a += 7;
            b += 7;
            c += 7;
            d += 7;
            e += 7;
            f += 7;
            g += 7;//System.out.println("A = " + a);//max a = 365 ("Понедельник")
        }
        System.out.print("This will be " + result);
    }

    public static void main(String[] args) {
        dayOfWeekend();
    }

}

