package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh05N010 {
    private static void printDollarsToRubles() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите текущий курс доллара $: ");
        double course = scanner.nextDouble();
        int lengthMas = 20;
        double masRubles[] = new double[lengthMas];
        int masDollars[] = new int[lengthMas];
        System.out.println("-------Table-------");
        System.out.println("$ - рубли");
        for (int i = 1; i <= masDollars.length; i++) {
            masDollars[i - 1] = i;
            masRubles[i - 1] = masDollars[i - 1] * course;
            System.out.println(masDollars[i - 1] + "\t" + masRubles[i - 1]);
        }
        System.out.println("-------------------");
    }

    public static void main(String[] args) {
        printDollarsToRubles();
    }
}
