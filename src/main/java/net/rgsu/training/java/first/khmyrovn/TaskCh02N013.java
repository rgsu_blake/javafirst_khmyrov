package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh02N013 {
    private static void reverseInt() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите трёхзначное число: ");
        int startInt = scanner.nextInt();
        System.out.println("Дано число: " + startInt);

        int currentInt = startInt;
        int newCurrentInt = 0;
        newCurrentInt += currentInt % 10 * 100;
        currentInt /= 10;
        newCurrentInt += currentInt % 10 * 10;
        currentInt /= 10;
        newCurrentInt += currentInt % 10;
        System.out.print("Данное число наоборот: ");
        System.out.println(newCurrentInt);
    }

    public static void main(String[] args) {
        reverseInt();
    }
}
