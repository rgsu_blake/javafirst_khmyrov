package net.rgsu.training.java.first.khmyrovn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TaskCh09N017 {
    private static void isExtrimsEquals() throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите слово: ");
        String s = r.readLine();
        boolean isTrue = false;
        if (s.toLowerCase().charAt(0) == s.toLowerCase().charAt(s.length() - 1))
            isTrue = true;
        System.out.println("Слово начинается и оканчивается на одну и ту же букву?");
        System.out.println(isTrue);
    }

    public static void main(String[] args) throws IOException {
        isExtrimsEquals();
    }
}
