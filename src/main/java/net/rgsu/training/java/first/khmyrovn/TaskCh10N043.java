package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh10N043 {
    private static int sumDigitsInDigit(int number) {
        if (number / 10 == 0)
            return number;
        else
            return sumDigitsInDigit(number / 10) + number % 10;
    }

    private static int amountOfDigits(int number) {
        if (number / 10 == 0)
            return 1;
        else
            return amountOfDigits(number / 10) + 1;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите натуральное число: ");
        int digit = scanner.nextInt();
        System.out.println("Сумма всех цифр равна\t" + sumDigitsInDigit(digit));
        System.out.println("Кол-во цифр в числе равно\t" + amountOfDigits(digit));
    }
}
