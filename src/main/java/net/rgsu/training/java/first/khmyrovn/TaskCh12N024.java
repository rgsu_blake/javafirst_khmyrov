package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh12N024 {
    private static void printMatrExprA(int len) {
        int mas[][] = new int[len][len];
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len; j++) {
                if (i == 0 || j == 0) {
                    mas[i][j] = 1;
                } else {
                    mas[i][j] = mas[i - 1][j] + mas[i][j - 1];
                }
            }
        }
        for (int i = 0; i < len; i++) {//-----Вывод матрицы-----
            for (int j = 0; j < len; j++) {
                System.out.print(mas[i][j] + "\t");
            }
            System.out.println();
        }
    }

    private static void printMatrExprB(int len) {
        int[] line = new int[len * len];
        for (int i = 0; i < len; i++) {
            line[i] = i + 1;
            line[i + len] = i + 1;
        }
        for (int i = 0; i < len; i++) {//-----Вывод матрицы-----
            for (int j = 0; j < len; j++) {
                System.out.print(line[i + j] + "\t");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размерность матрицы: ");
        int sizeMatrix = scanner.nextInt();
        printMatrExprA(sizeMatrix);
        System.out.println();
        printMatrExprB(sizeMatrix);
    }
}
