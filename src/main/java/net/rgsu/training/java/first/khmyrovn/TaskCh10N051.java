package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh10N051 {
    private static void exprA(int n) {
        if (n > 0) {
            System.out.println("n = " + n);
            exprA(n - 1);
        }
    }

    private static void exprB(int n) {
        if (n > 0) {
            exprB(n - 1);
            System.out.println("n = " + n);
        }
    }

    private static void exprC(int n) {
        if (n > 0) {
            System.out.println("n = " + n);
            exprC(n - 1);
            System.out.println("n = " + n);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите n в выражении А: ");
        exprA(scanner.nextInt());
        System.out.println("Введите n в выражении Б: ");
        exprB(scanner.nextInt());
        System.out.println("Введите n в выражении В: ");
        exprC(scanner.nextInt());
    }
}
