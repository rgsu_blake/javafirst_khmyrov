package net.rgsu.training.java.first.khmyrovn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TaskCh09N166 {
    private static void reverseFirstLastWord() throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Напишите предложение: ");
        String sentence = r.readLine();
        String mas[] = sentence.split(" ");
        String curString = mas[0];
        mas[0] = mas[mas.length - 1];
        mas[mas.length - 1] = curString;
        for (int i = 0; i < mas.length; i++)
            System.out.print(mas[i] + " ");
    }

    public static void main(String[] args) throws IOException {
        reverseFirstLastWord();
    }
}
