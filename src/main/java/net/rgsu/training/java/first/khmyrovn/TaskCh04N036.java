package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh04N036 {
    private static void colorOfTrafficLight() {
        Scanner scanner = new Scanner(System.in);
        String color;
        System.out.println("Введите ,сколько минут прошло с начала часа ");
        double min = scanner.nextDouble();//время в минутах с начала часа
        double i, j;
        boolean isGreen = false;
        if (min == 60.0)
            isGreen = true;
        for (i = 0.0, j = 3.0; i < 60.0; i += 5.0, j += 5.0) {
            if (min >= i & min < j) {
                isGreen = true;
                break;
            }
        }
        if (isGreen)
            color = "Green";
        else
            color = "Red";
        System.out.println("Сигнал светофора для пешеходов в данный момент\t");
        System.out.println(color + "!");
    }

    public static void main(String[] args) {
        colorOfTrafficLight();
    }
}
