package net.rgsu.training.java.first.khmyrovn;

public class TaskCh12N023 {
    private static void writeMasExprA(int n) {
        int mas[][] = new int[n][n];
        int len = n - 2;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j || i + j == len + 1) {
                    mas[i][j] = 1;
                } else {
                    mas[i][j] = 0;
                }
            }
        }
        for (int i = 0; i < n; i++) {//-----Вывод матрицы-----
            for (int j = 0; j < n; j++) {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void writeMasExprB(int n) {
        int mas[][] = new int[n][n];
        int len = n - 2;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j || i + j == len + 1 || (i == 2 & j == 3) || (i == 3 & j == 2) || (i == 3 & j == 4) || (i == 4 & j == 3)) {
                    mas[i][j] = 1;
                } else {
                    mas[i][j] = 0;
                }
            }
        }
        for (int i = 0; i < n; i++) {//-----Вывод матрицы-----
            for (int j = 0; j < n; j++) {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void writeMasExprC(int n) {
        int[][] mas = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (((j >= i) & (i + j <= 6)) || ((j <= i) & (i + j >= 6))) {
                    mas[i][j] = 1;

                } else {
                    mas[i][j] = 0;
                }
            }
        }
        for (int i = 0; i < n; i++) {//-----Вывод матрицы-----
            for (int j = 0; j < n; j++) {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        int sizeMas = 7;
        writeMasExprA(sizeMas);
        System.out.println();
        writeMasExprB(sizeMas);
        System.out.println();
        writeMasExprC(sizeMas);
    }
}
