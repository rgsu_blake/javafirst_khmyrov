package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh01N003 {
    private static void printInputDigit() {
        Scanner s = new Scanner(System.in);
        long number = s.nextLong();
        System.out.println("Вы ввели число " + number);
    }

    public static void main(String[] args) {
        printInputDigit();
    }
}
