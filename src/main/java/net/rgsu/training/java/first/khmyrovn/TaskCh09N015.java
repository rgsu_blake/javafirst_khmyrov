package net.rgsu.training.java.first.khmyrovn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TaskCh09N015 {
    private static void printKDigit() throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите слово :");
        String ourWord = r.readLine();
        System.out.println("Укажите k-ый индекс в слове: ");
        String s = r.readLine();
        int indexDigit = Integer.parseInt(s);
        System.out.println(ourWord.charAt(indexDigit));
    }

    public static void main(String[] args) throws IOException {
        printKDigit();
    }
}
