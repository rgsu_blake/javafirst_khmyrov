package net.rgsu.training.java.first.khmyrovn;

public class TaskCh03N026 {
    private static boolean isAExpr(boolean x, boolean y, boolean z) {
        boolean subExpr1;
        boolean subExpr2;
        boolean res;
        subExpr1 = !(x | y);
        subExpr2 = !x | !z;
        res = subExpr1 & subExpr2;
        if (!x & !y) {
            if (!z)
                System.out.println("000\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
            else
                System.out.println("001\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
        } else if (!x & y) {
            if (!z)
                System.out.println("010\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
            else
                System.out.println("011\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
        } else if (!y) {
            if (!z)
                System.out.println("100\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
            else
                System.out.println("101\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
        } else
            System.out.println("111\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
        return res;
    }

    private static boolean isBExpr(boolean x, boolean y, boolean z) {
        boolean subExpr1;
        boolean subExpr2;
        boolean res;
        subExpr1 = !(!x & y);
        subExpr2 = x & !z;
        res = subExpr1 | subExpr2;
        if (!x & !y) {
            if (!z)
                System.out.println("000\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
            else
                System.out.println("001\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
        } else if (!x & y) {
            if (!z)
                System.out.println("010\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
            else
                System.out.println("011\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
        } else if (!y) {
            if (!z)
                System.out.println("100\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
            else
                System.out.println("101\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
        } else
            System.out.println("111\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
        return res;
    }

    private static boolean isCExpr(boolean x, boolean y, boolean z) {
        boolean subExpr1;
        boolean subExpr2;
        boolean res;
        subExpr1 = !(x | !z);
        subExpr2 = !y & subExpr1;
        res = x | subExpr2;
        if (!x & !y) {
            if (!z)
                System.out.println("000\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
            else
                System.out.println("001\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
        } else if (!x & y) {
            if (!z)
                System.out.println("010\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
            else
                System.out.println("011\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
        } else if (!y) {
            if (!z)
                System.out.println("100\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
            else
                System.out.println("101\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
        } else
            System.out.println("111\t" + subExpr1 + "\t" + subExpr2 + "\t\t" + res);
        return res;
    }

    public static void main(String[] args) {
        System.out.println("xyz\t!(x|y)\t!x|!z\tresult \" & \"");
        isAExpr(false, false, false);//000
        isAExpr(false, false, true);//001
        isAExpr(false, true, false);//010
        isAExpr(false, true, true);//011
        isAExpr(true, false, false);//100
        isAExpr(true, false, true);//101
        isAExpr(true, true, false);//110
        isAExpr(true, true, true);//111
        System.out.println("------------------------------------------------------");
        System.out.println("xyz\t!(!x&y)\tx&!z\tresult \" | \"");
        isBExpr(false, false, false);//000
        isBExpr(false, false, true);//001
        isBExpr(false, true, false);//010
        isBExpr(false, true, true);//011
        isBExpr(true, false, false);//100
        isBExpr(true, false, true);//101
        isBExpr(true, true, false);//110
        isBExpr(true, true, true);//111
        System.out.println("------------------------------------------------------");
        System.out.println("xyz\t!(x|!z)\t!y&subExp1\tresult \"x | expr2\"");
        isCExpr(false, false, false);//000
        isCExpr(false, false, true);//001
        isCExpr(false, true, false);//010
        isCExpr(false, true, true);//011
        isCExpr(true, false, false);//100
        isCExpr(true, false, true);//101
        isCExpr(true, true, false);//110
        isCExpr(true, true, true);//111
    }
}
