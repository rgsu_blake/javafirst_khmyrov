package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh10N053 {
    private static void reverseDigit(int num) {
        if (num % 10 != 0) {
            System.out.print(num % 10);
            reverseDigit(num / 10);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder str = new StringBuilder();
        System.out.println("Введите последовательность чисел,окончание это число 0!");
        do {
            int x = scanner.nextInt();
            str.append(x);
        }
        while (!str.toString().contains("0"));
        String str1 = str.toString();
        int number = Integer.parseInt(str1);
        reverseDigit(number / 10);
    }
}
