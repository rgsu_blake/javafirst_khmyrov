package net.rgsu.training.java.first.khmyrovn;

public class TaskCh05N038 {
    private static void rangeKmFromHome() {
        double sum = 0.0;
        for (int i = 1; i <= 100; i++) {
            if (i % 2 == 0)
                sum -= 1.0 / i;
            else
                sum += 1.0 / i;
        }
        System.out.println("He's located on\n" + sum + " km from home!");
    }

    private static void sumKmAll() {
        double sum = 0.0;
        for (int i = 1; i <= 100; i++) {
            sum += 1.0 / i;
        }
        System.out.println(sum + " km he went in total!");
    }

    public static void main(String[] args) {
        rangeKmFromHome();
        sumKmAll();
    }
}
