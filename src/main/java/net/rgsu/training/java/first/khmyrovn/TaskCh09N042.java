package net.rgsu.training.java.first.khmyrovn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TaskCh09N042 {
    private static void printReverseWord() throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите слово: ");
        StringBuilder str = new StringBuilder(r.readLine());
        System.out.println(str.reverse());
    }

    public static void main(String[] args) throws IOException {
        printReverseWord();
    }
}
