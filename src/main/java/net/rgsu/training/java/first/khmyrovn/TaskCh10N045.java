package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh10N045 {
    private static double findNElement(double first, double d, int n) {
        if (n == 1)
            return first;
        else
            return findNElement(first, d, n - 1) + d;
    }

    private static double findSumNFirstElement(double first, double d, int n) {
        if (n == 1)
            return first;
        else
            return findSumNFirstElement(first, d, n - 1) + findNElement(first, d, n);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первый член арифметической прогрессии a0: ");
        double first = scanner.nextDouble();
        System.out.println("Введите разность арифметической прогресии d: ");
        double difference = scanner.nextDouble();
        System.out.println("Введите номер n-го элемента прогресии: n ");
        int number = scanner.nextInt();
        System.out.println("Введите n первых элементов прогресии: n ");
        int number1 = scanner.nextInt();
        System.out.println(findNElement(first, difference, number));
        System.out.println(findSumNFirstElement(first, difference, number1));
    }
}
