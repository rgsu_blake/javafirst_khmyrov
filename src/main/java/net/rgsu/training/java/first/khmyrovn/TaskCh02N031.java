package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh02N031 {
    private static void exprReverse() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите трёхзначное число: ");
        int number = scanner.nextInt();
        while ((number < 100) || (number > 999)) {
            System.out.println("Введите N в промежутке от 100 до 999");
            number = scanner.nextInt();
        }
        System.out.println("Число N = " + number);
        int newN = number / 10;
        int ostN = number % 10;//Other part of a division
        int x = number / 100 * 100 + ostN * 10 + newN % 10;
        System.out.println("Число X = " + x);
    }

    public static void main(String[] args) {
        exprReverse();
    }
}
