package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh10N044 {
    private static int findSumDigits(int digit) {
        if (digit / 10 == 0)
            return digit;
        else
            return findSumDigits(digit / 10) + digit % 10;
    }

    private static int findDigitSquare(int number) {
        if (number / 10 == 0)
            return number;
        else
            return findDigitSquare(number / 10) + number % 10;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите натуральное число: ");
        int digit = scanner.nextInt();
        int result = findSumDigits(digit);
        int squareDigit = findDigitSquare(result);
        System.out.println("Цифровой корень равен " + squareDigit);
    }
}
