package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh04N033 {
    private static void isEvenNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите натуральное число: ");
        int natureNumber = scanner.nextInt();
        System.out.print("Верно ли, что натуральное число " + natureNumber);
        System.out.println(" заканчивается четной цифрой?");
        if (natureNumber % 2 == 0)
            System.out.println("true");
        else
            System.out.println("false");
    }

    private static void isOddNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите натуральное число: ");
        int natureNumber = scanner.nextInt();
        System.out.print("Верно ли, что натуральное число " + natureNumber);
        System.out.println(" заканчивается нечетной цифрой?");
        if (natureNumber % 2 != 0)
            System.out.println("true");
        else
            System.out.println("false");
    }

    public static void main(String[] args) {
        isEvenNumber();
        isOddNumber();
    }
}
