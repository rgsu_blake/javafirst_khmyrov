package net.rgsu.training.java.first.khmyrovn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TaskCh09N107 {
    private static void reverseFistAWithLastO() throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите слово: ");
        String word = r.readLine();
        char[] mas = word.toLowerCase().toCharArray();
        int indexFirstA = word.toLowerCase().indexOf("а");//индекс первой буквы А в слове
        char digitA = 'а';
        int indexLastO = word.toLowerCase().lastIndexOf("о");//индекс последней буквы О в слове
        for (int i = 0; i < mas.length; i++) {
            if (i == indexFirstA)
                mas[i] = mas[indexLastO];
            if (i == indexLastO)
                mas[i] = digitA;
        }
        for (int i = 0; i < mas.length; i++) {
            System.out.print(mas[i]);
        }
    }

    public static void main(String[] args) throws IOException {
        reverseFistAWithLastO();
    }
}
