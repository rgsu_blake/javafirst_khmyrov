package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh10N052 {
    private static void reverseNumber(int num) {
        if (num % 10 != 0) {
            System.out.print(num % 10);
            reverseNumber(num / 10);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите натуральное число:");
        int digit = scanner.nextInt();
        System.out.println("Число в обратном порядке:\t");
        reverseNumber(digit);
    }
}
