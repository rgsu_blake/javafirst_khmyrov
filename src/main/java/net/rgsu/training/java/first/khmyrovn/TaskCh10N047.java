package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh10N047 {
    private static int findNElementFib(int n) {
        if (n == 1 || n == 2) {
            return 1;
        } else {
            return findNElementFib(n - 1) + findNElementFib(n - 2);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите номер элемента последовательности Фибоначчи: n");
        int number = scanner.nextInt();
        System.out.println(findNElementFib(number));
    }
}
