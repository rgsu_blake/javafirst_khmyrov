package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh02N039 {
    private static void myMethod() {
        int hours;
        int minutes;
        int seconds;
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите текущее время...\nЧасы:\t");
        hours = scan.nextInt();
        while (0 > hours || hours > 23)
            hours = scan.nextInt();
        System.out.println("Минуты:");
        minutes = scan.nextInt();
        while (0 > minutes || minutes > 59)
            minutes = scan.nextInt();
        System.out.println("Секунды:");
        seconds = scan.nextInt();
        while (0 > seconds || seconds > 59)
            seconds = scan.nextInt();
        double gradResult;//Range in grades * between the first and current position Hour`seconds Hand.
        double gradH = 30.0;
        double gradM = 0.5;
        double gradS = 0.0083;
        gradResult = hours * gradH + minutes * gradM + seconds * gradS;
        System.out.println("Угол часовой стрелки в градусах равен :\t" + gradResult);
    }

    public static void main(String[] args) {
        myMethod();
    }
}
