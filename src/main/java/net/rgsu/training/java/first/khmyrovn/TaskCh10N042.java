package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh10N042 {
    private static double doublePow(double dig, int degree) {
        if (degree == 0)
            return 1.0;
        else if (degree == 1)
            return dig;
        else if (degree == 2)
            return dig * dig;
        else
            return doublePow(dig, degree - 1) * dig;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите вещественное число: ");
        double digit = scanner.nextDouble();
        System.out.println("Введите натуральную степень, в которую необходимо возвести: ");
        int degreeOfDigit = scanner.nextInt();
        double result = doublePow(digit, degreeOfDigit);
        System.out.println(result);
    }
}
