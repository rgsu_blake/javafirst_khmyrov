package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh10N049 {
    private static int findIndexMax(int[] mas, int indMax) {
        if (mas.length > indMax) {
            int max = findIndexMax(mas, indMax + 1);
            return (mas[indMax] > max ? indMax : max);
        } else {
            return 0;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размерность массива: ");
        int length = scanner.nextInt();
        int mas[] = new int[length];
        System.out.println("Введите элементы массива: ");
        for (int i = 0; i < length; i++) {
            mas[i] = scanner.nextInt();
        }
        System.out.println("Индекс max-го элемента: " + findIndexMax(mas, 0));
    }
}
