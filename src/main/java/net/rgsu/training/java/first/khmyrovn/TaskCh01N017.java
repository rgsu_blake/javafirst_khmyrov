package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh01N017 {
    private static double exrO(double x) {
        return Math.sqrt(1 - Math.pow(Math.sin(x), 2));
    }

    private static double exrP(double x, double a, double b, double c) {
        return 1 / Math.sqrt(a * Math.pow(x, 2) + b * x + c);
    }

    private static double exrR(double x) {
        return (Math.sqrt(x + 1) + Math.sqrt(x - 1)) / 2 * Math.sqrt(x);
    }

    private static double exrS(double x) {
        return Math.abs(x) + Math.abs(x + 1);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число X: ");
        double x = scanner.nextDouble();
        exrO(x);
        exrP(x, 1, 2, 3);
        exrR(x);
        exrS(x);
    }
}
