package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh10N048 {
    private static int findMaxElementInMas(int[] mas, int n) {
        if (mas.length > n) {
            int max = findMaxElementInMas(mas, n + 1);
            return (mas[n] > max) ? mas[n] : max;
        } else {
            return mas[0];
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размерность массива: ");
        int length = scanner.nextInt();
        int mas[] = new int[length];
        System.out.println("Введите элементы массива: ");
        for (int i = 0; i < length; i++) {
            mas[i] = scanner.nextInt();
        }
        System.out.println("Max element is " + findMaxElementInMas(mas, 0));
    }
}
