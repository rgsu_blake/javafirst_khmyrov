package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh02N043 {
    private static void printExpression() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите целое число a: ");
        int a = scanner.nextInt();
        System.out.println("Введите целое число b: ");
        int b = scanner.nextInt();
        int c1 = a % b;
        int c2 = b % a;
        System.out.println(c1 * c2 + 1);
    }

    public static void main(String[] args) {
        printExpression();
    }
}
