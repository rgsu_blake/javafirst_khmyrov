package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh10N050 {
    private static int funcAkerman(int a, int b) {
        int result = 0;
        if (a > 0 & b > 0) {
            result = funcAkerman(a - 1, funcAkerman(a, b - 1));
        } else if (a == 0) {
            result = b + 1;
        } else if (b == 0) {
            result = funcAkerman(a - 1, 1);
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите аргументы функции Аккермана\nA n,m ");
        System.out.println("n: ");
        int n = scanner.nextInt();
        System.out.println("m: ");
        int m = scanner.nextInt();
        System.out.println("Result = " + funcAkerman(n, m));
    }
}
