package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh04N115 {
    private static String findAnimal(int y) {
        String animal = null;
        int count = y % 12;
        switch (count) {
            case 0:
                animal = "Обезьяна";
                break;
            case 1:
                animal = "Петух";
                break;
            case 2:
                animal = "Собака";
                break;
            case 3:
                animal = "Свинья";
                break;
            case 4:
                animal = "Крыса";
                break;
            case 5:
                animal = "Корова";
                break;
            case 6:
                animal = "Тигр";
                break;
            case 7:
                animal = "Заяц";
                break;
            case 8:
                animal = "Дракон";
                break;
            case 9:
                animal = "Змея";
                break;
            case 10:
                animal = "Лошадь";
                break;
            case 11:
                animal = "Овца";
                break;

        }
        return animal;
    }

    private static String findColor(int y) {
        String color = null;
        int count = y % 10;
        switch (count) {
            case 0:
            case 1:
                color = "Белый";
                break;
            case 2:
            case 3:
                color = "Чёрный";
                break;
            case 4:
            case 5:
                color = "Зеленый";
                break;
            case 6:
            case 7:
                color = "Красный";
                break;
            case 8:
            case 9:
                color = "Жёлтый";
                break;

        }
        return color;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите номер года: ");
        int year = scanner.nextInt();
        System.out.print(findAnimal(year) + ", " + findColor(year));
    }
}
