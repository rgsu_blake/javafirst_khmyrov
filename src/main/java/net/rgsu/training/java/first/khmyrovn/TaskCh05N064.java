package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh05N064 {
    private static void avgPopulationDensity() {
        Scanner scanner = new Scanner(System.in);
        int[] masPopulation = new int[12];
        double[] masSquare = new double[12];
        for (int i = 0; i < masPopulation.length - 1; i++) {
            System.out.println("Введите численность " + (i + 1) + "-го района в тыс.чел. :");
            masPopulation[i] = scanner.nextInt();
            System.out.println("Введите площадь " + (i + 1) + "-го района в кв.километрах :");
            masSquare[i] = scanner.nextDouble();
        }

        double[] masAvgDensity = new double[12];
        double sum = 0.0;
        for (int i = 0; i < masAvgDensity.length - 1; i++) {
            masAvgDensity[i] = masPopulation[i] * 1000 / masSquare[i];
            sum += masAvgDensity[i];
        }
        System.out.print("Cредняя плотность населения по области : ");
        System.out.println(sum / masAvgDensity.length);
    }

    public static void main(String[] args) {
        avgPopulationDensity();
    }
}
