package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh10N046 {
    private static double findNElement(double first, double q, int n) {
        if (n == 1) {
            return first;
        } else {
            return findNElement(first, q, n - 1) * q;
        }
    }

    private static double findSumFirstNElements(double first, double q, int n) {
        if (n == 1) {
            return first;
        } else {
            return findSumFirstNElements(first, q, n - 1) + findNElement(first, q, n);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первый член геометрической прогрессии b0: ");
        double first = scanner.nextDouble();
        System.out.println("Введите знаменатель геометрической прогресии q: ");
        double difference = scanner.nextDouble();
        System.out.println("Введите номер n-го элемента прогресии: n ");
        int number = scanner.nextInt();
        System.out.println("Введите номер суммы первых n элементов прогресии: n ");
        int sumFirst = scanner.nextInt();

        System.out.println(findNElement(first, difference, number));
        System.out.println(findSumFirstNElements(first, difference, sumFirst));
    }
}
