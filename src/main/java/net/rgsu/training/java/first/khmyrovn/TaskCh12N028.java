package net.rgsu.training.java.first.khmyrovn;

import java.util.Scanner;

public class TaskCh12N028 {
    private static void printMatrExprB(int len) {
        int mas[][] = new int[len][len];
        int row = 0;
        int col = 0;
        int dx = 1;
        int dy = 0;
        int dirChanges = 0;
        int visits = len;

        for (int i = 0; i < len * len; i++) {
            mas[row][col] = i + 1;
            if (--visits == 0) {
                visits = len * (dirChanges % 2) +
                        len * ((dirChanges + 1) % 2) -
                        (dirChanges / 2 - 1) - 2;
                int temp = dx;
                dx = -dy;
                dy = temp;
                dirChanges++;
            }
            col += dx;
            row += dy;
        }
        for (int i = 0; i < len; i++) {//-----Вывод матрицы-----
            for (int j = 0; j < len; j++) {
                System.out.print(mas[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размерность матрицы: ");
        int sizeMatrix = scanner.nextInt();
        System.out.println();
        printMatrExprB(sizeMatrix);
    }
}
